package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class LiyuanSobaTest {
    private Class<?> liyuanSobaClass;
    private Class<?> nameClass;
    private LiyuanSoba liyuanSoba;

    @BeforeEach
    public void setup() throws Exception {
        liyuanSobaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba");
        nameClass = Class.forName("java.lang.String");
        liyuanSoba = new LiyuanSoba("mockName");
    }

    @Test
    public void testLiyuanSobaIsConcreteClass() {
        assertFalse(Modifier.isAbstract(liyuanSobaClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIsASubclassOfMenu() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu", liyuanSobaClass.getSuperclass().getTypeName());
    }

    @Test
    public void testLiyuanSobaConstructorReceivesNameAsParameter() {
        Class<?>[] classArg = new Class[1];
        classArg[0] = nameClass;

        Collection<Constructor<?>> constructors = Arrays.asList(
                liyuanSobaClass.getDeclaredConstructors());

        assertTrue(constructors.stream()
                .anyMatch(type -> Arrays.equals(
                        type.getParameterTypes(), classArg)));
    }

    @Test
    public void testLiyuanSobaGetNameMethod() {
        assertEquals("java.lang.String", liyuanSoba.getName().getClass().getTypeName());
        assertEquals("mockName", liyuanSoba.getName());
    }

    @Test
    public void testLiyuanSobaGetNoodleMethod() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba",
                liyuanSoba.getNoodle().getClass().getTypeName());
    }

    @Test
    public void testLiyuanSobaGetMeatMethod() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef",
                liyuanSoba.getMeat().getClass().getTypeName());
    }

    @Test
    public void testLiyuanSobaGetToppingMethod() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom",
                liyuanSoba.getTopping().getClass().getTypeName());
    }

    @Test
    public void testLiyuanSobaGetFlavorMethod() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet",
                liyuanSoba.getFlavor().getClass().getTypeName());
    }
}
