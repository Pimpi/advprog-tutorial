package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class UmamiFlavorTest {
    private Class<?> umamiFlavorClass;

    @BeforeEach
    public void setup() throws Exception {
        umamiFlavorClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami");
    }

    @Test
    public void testUmamiFlavorIsConcreteClass() {
        assertFalse(Modifier.isAbstract(umamiFlavorClass.getModifiers()));
    }

    @Test
    public void testUmamiFlavorIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(umamiFlavorClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor")));
    }

    @Test
    public void testUmamiFlavorOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = umamiFlavorClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0, getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testUmamiFlavorGetDescripton() {
        Umami umami = new Umami();

        assertEquals("Adding WanPlus Specialty MSG flavoring...", umami.getDescription());
    }
}
