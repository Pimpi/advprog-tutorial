package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class MenuServiceImplTest {
    private Class<?> menuServiceImplClass;
    private MenuServiceImpl menuServiceImpl;


    @BeforeEach
    public void setup() throws Exception {
        menuServiceImplClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl");
        menuServiceImpl = new MenuServiceImpl();
    }

    @Test
    public void testMenuServiceImplIsAConcreteClass() {
        assertFalse(Modifier.isAbstract(menuServiceImplClass.getModifiers()));
    }

    @Test
    public void testMenuServiceImplIsAMenuService() {
        Collection<Type> interfaces = Arrays.asList(menuServiceImplClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.service.MenuService")));
    }

    @Test
    public void testMenuServiceImplGetMenusMethod() throws Exception {
        Method getMenus = menuServiceImplClass.getDeclaredMethod("getMenus");

        assertEquals("java.util.List<id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu>",
                getMenus.getGenericReturnType().getTypeName());
        assertEquals(0, getMenus.getParameterCount());
        assertTrue(Modifier.isPublic(getMenus.getModifiers()));
    }

    @Test
    public void testMenuServiceImplGetMenusReturnListOfMenus() {
        MenuRepository mockRepo = new MenuRepository();
        MenuServiceImpl mockMenuServiceImpl = new MenuServiceImpl(mockRepo);

        assertEquals(mockRepo.getMenus(), mockMenuServiceImpl.getMenus());
    }

    @Test
    public void testMenuServiceImplCreateMenuInuzumaRamen() {
        Menu menu = menuServiceImpl.createMenu("mockName", "InuzumaRamen");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen",
                menu.getClass().getTypeName());
    }

    @Test
    public void testMenuFactoryImplMakeMenuLiyuanSoba() {
        Menu menu = menuServiceImpl.createMenu("mockName", "LiyuanSoba");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba",
                menu.getClass().getTypeName());
    }

    @Test
    public void testMenuFactoryImplMakeMenuMondoUdon() {
        Menu menu = menuServiceImpl.createMenu("mockName", "MondoUdon");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon",
                menu.getClass().getTypeName());
    }

    @Test
    public void testMenuFactoryImplMakeMenuSnevnezhaShirataki() {
        Menu menu = menuServiceImpl.createMenu("mockName", "SnevnezhaShirataki");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki",
                menu.getClass().getTypeName());
    }
}
