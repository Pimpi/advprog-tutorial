package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class PorkTest {
    private Class<?> porkClass;

    @BeforeEach
    public void setup() throws Exception {
        porkClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork");
    }

    @Test
    public void testPorkIsConcreteClass() {
        assertFalse(Modifier.isAbstract(porkClass.getModifiers()));
    }

    @Test
    public void testPorkIsAMeat() {
        Collection<Type> interfaces = Arrays.asList(porkClass.getInterfaces());

        assertTrue(interfaces.stream()
            .anyMatch(type -> type.getTypeName()
                .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat")));
    }

    @Test
    public void testPorkOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = porkClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String", getDescription.getGenericReturnType().getTypeName());
        assertEquals(0, getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(porkClass.getModifiers()));
    }

    @Test
    public void testPorkGetDescription() {
        Pork pork = new Pork();

        assertEquals("Adding Tian Xu Pork Meat...", pork.getDescription());
    }
}
