package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.test.util.ReflectionTestUtils;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class MenuRepositoryTest {
    Class<?> menuRepositoryClass;
    private MenuRepository menuRepository;

    @Mock
    List<Menu> listMenu;

    private Menu mockMenu1;
    private Menu mockMenu2;

    @BeforeEach
    public void setup() throws Exception {
        menuRepositoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository");
        menuRepository = new MenuRepository();
        listMenu = new ArrayList<>();
        mockMenu1 = new InuzumaRamen("mockName1");
        mockMenu2 = new LiyuanSoba("mockName2");
        listMenu.add(mockMenu1);
        listMenu.add(mockMenu2);
    }

    @Test
    public void testMenuRepositoryIsPublic() {
        assertTrue(Modifier.isPublic(menuRepositoryClass.getModifiers()));
    }

    @Test
    public void testMenuRepositoryGetMenusMethod() throws Exception {
        Method getMenus = menuRepositoryClass.getDeclaredMethod("getMenus");

        assertEquals("java.util.List<id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu>",
                getMenus.getGenericReturnType().getTypeName());
        assertEquals(0, getMenus.getParameterCount());
        assertTrue(Modifier.isPublic(getMenus.getModifiers()));
    }

    @Test
    public void testMenuRepositoryGetMenusReturnListOfMenus() {
        ReflectionTestUtils.setField(menuRepository, "list", listMenu);

        List<Menu> acquiredMenu = menuRepository.getMenus();

        assertThat(acquiredMenu).isEqualTo(listMenu);
    }

    @Test
    public void testMenuRepositoryAdd() {
        assertEquals(0, menuRepository.getMenus().size());
        menuRepository.add(mockMenu1);
        assertEquals(1, menuRepository.getMenus().size());
        menuRepository.add(mockMenu2);
        assertEquals(2, menuRepository.getMenus().size());
        assertTrue(menuRepository.getMenus().contains(mockMenu1));
        assertTrue(menuRepository.getMenus().contains(mockMenu2));
    }
}