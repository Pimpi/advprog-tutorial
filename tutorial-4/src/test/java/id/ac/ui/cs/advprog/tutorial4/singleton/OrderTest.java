package id.ac.ui.cs.advprog.tutorial4.singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;


import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class OrderTest {

    private Class<?> orderDrinkClass;
    private OrderDrink orderDrink;

    private Class<?> orderFoodClass;
    private OrderFood orderFood;

    @BeforeEach
    public void setUp() throws Exception {
        orderDrinkClass = Class.forName(OrderDrink.class.getName());
        orderDrink = OrderDrink.getInstance();

        orderFoodClass = Class.forName(OrderFood.class.getName());
        orderFood = OrderFood.getInstance();
    }

    @Test
    public void testNoPublicConstructors() {
        List<Constructor> constructors = Arrays.asList(orderDrinkClass.getDeclaredConstructors());

        boolean check = constructors.stream()
                .anyMatch(c -> !Modifier.isPrivate(c.getModifiers()));

        assertEquals(false, check);
    }

    @Test
    public void testNoPublicConstructors2() {
        List<Constructor> constructors = Arrays.asList(orderFoodClass.getDeclaredConstructors());

        boolean check = constructors.stream()
                .anyMatch(c -> !Modifier.isPrivate(c.getModifiers()));

        assertEquals(false, check);
    }


    @Test
    public void testGetInstanceShouldReturnSingletonInstance() {
        OrderDrink orderDrink = OrderDrink.getInstance();

        assertNotNull(orderDrink);
    }

    @Test
    public void testGetInstanceShouldReturnSingletonInstance2() {
        OrderFood orderFood = OrderFood.getInstance();

        assertNotNull(orderFood);
    }

    @Test
    public void testOrderDrink(){
        orderDrink.setDrink("Bir Bintang");
        assertEquals("Bir Bintang", orderDrink.getDrink());
    }

    @Test
    public void testOrderFood(){
        orderFood.setFood("Tonkotsu Ramen");
        assertEquals("Tonkotsu Ramen", orderFood.getFood());
    }

    @Test
    public void testOrderDrinkExist(){
        assertNotNull(orderDrink.toString());
    }

    @Test
    public void testOrderFoodExist(){
        assertNotNull(orderFood.toString());
    }

    @Test
    public void testOrderDrinkIsPublicAndNotAbstractClass() {
        assertTrue(Modifier.isPublic(orderDrinkClass.getModifiers()));
        assertFalse(Modifier.isAbstract(orderDrinkClass.getModifiers()));
    }

    @Test
    public void testOrderFoodIsPublicAndNotAbstractClass() {
        assertTrue(Modifier.isPublic(orderFoodClass.getModifiers()));
        assertFalse(Modifier.isAbstract(orderFoodClass.getModifiers()));
    }

    @Test
    public void testOrderDrinkInstantiationInterruptedException() {
        Thread thread = new Thread();
        InterruptedException interruptedException = new InterruptedException();
        try {
            OrderDrink.getInstance();
            thread.interrupt();
        } catch (IllegalStateException e) {
            assertSame(interruptedException, e.getCause());
        }
    }

    @Test
    public void testOrderFoodInstantiationInterruptedException() {
        Thread thread = new Thread();
        InterruptedException interruptedException = new InterruptedException();
        try {
            thread.start();
            OrderFood.getInstance();
            thread.interrupt();
        } catch (IllegalStateException e) {
            assertSame(interruptedException, e.getCause());
        }
    }

    @Test
    public void testOrderDrinkIsSingleton() {
        assertSame(OrderDrink.getInstance(), OrderDrink.getInstance());
    }

    @Test
    public void testOrderFoodIsSingleton() {
        assertSame(OrderFood.getInstance(), OrderFood.getInstance());
    }

    @Test
    public void testOrderDrinkGetDrinkMethod() throws Exception {
        Method getDrink = orderDrinkClass.getDeclaredMethod("getDrink");

        assertTrue(Modifier.isPublic(getDrink.getModifiers()));
        assertEquals(0, getDrink.getParameterCount());
        assertEquals("java.lang.String",
                getDrink.getGenericReturnType().getTypeName());
    }

    @Test
    public void testOrderFoodGetDrinkMethod() throws Exception {
        Method getFood = orderFoodClass.getDeclaredMethod("getFood");

        assertTrue(Modifier.isPublic(getFood.getModifiers()));
        assertEquals(0, getFood.getParameterCount());
        assertEquals("java.lang.String",
                getFood.getGenericReturnType().getTypeName());
    }

    @Test
    public void testOrderDrinkSetDrinkMethod() throws Exception {
        Method setDrink = orderDrinkClass.getDeclaredMethod("setDrink", String.class);

        assertTrue(Modifier.isPublic(setDrink.getModifiers()));
        assertEquals(1, setDrink.getParameterCount());
        assertEquals("void",
                setDrink.getGenericReturnType().getTypeName());
    }

    @Test
    public void testOrderFoodSetDrinkMethod() throws Exception {
        Method setFood = orderFoodClass.getDeclaredMethod("setFood", String.class);

        assertTrue(Modifier.isPublic(setFood.getModifiers()));
        assertEquals(1, setFood.getParameterCount());
        assertEquals("void",
                setFood.getGenericReturnType().getTypeName());
    }

    @Test
    public void testOrderDrinkSetDrinkReceivesStringAsParameter() throws Exception {
        Class<?>[] classArg = new Class[1];
        classArg[0] = String.class;

        Collection<Method> method = Arrays.asList(
                orderDrinkClass.getDeclaredMethod("setDrink", String.class));

        assertTrue(method.stream()
                .anyMatch(type -> Arrays.equals(
                        type.getParameterTypes(), classArg)));
    }

    @Test
    public void testOrderFoodSetDrinkReceivesStringAsParameter() throws Exception {
        Class<?>[] classArg = new Class[1];
        classArg[0] = String.class;

        Collection<Method> method = Arrays.asList(
                orderFoodClass.getDeclaredMethod("setFood", String.class));

        assertTrue(method.stream()
                .anyMatch(type -> Arrays.equals(
                        type.getParameterTypes(), classArg)));
    }

    @Test
    public void testOrderDrinkToStringMethod() throws Exception {
        Method toString = orderDrinkClass.getDeclaredMethod("toString");

        assertTrue(Modifier.isPublic(toString.getModifiers()));
        assertEquals(0, toString.getParameterCount());
        assertEquals("java.lang.String",
                toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testOrderFoodToStringMethod() throws Exception {
        Method toString = orderFoodClass.getDeclaredMethod("toString");

        assertTrue(Modifier.isPublic(toString.getModifiers()));
        assertEquals(0, toString.getParameterCount());
        assertEquals("java.lang.String",
                toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testOrderDrinkToString(){
        orderDrink.setDrink("Bir Bintang");
        assertEquals("Bir Bintang", orderDrink.toString());
    }

    @Test
    public void testOrderFoodToString(){
        orderFood.setFood("Tonkotsu Ramen");
        assertEquals("Tonkotsu Ramen", orderFood.toString());
    }
}
