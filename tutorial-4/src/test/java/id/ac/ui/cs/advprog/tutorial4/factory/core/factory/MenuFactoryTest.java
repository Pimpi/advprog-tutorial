package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class MenuFactoryTest {
    private Class<?> menuFactoryClass;
    private Class<?> name;
    private Class<?> type;

    @BeforeEach
    public void setup() throws Exception {
        menuFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory");
        name = Class.forName("java.lang.String");
        type = Class.forName("java.lang.String");
    }

    @Test
    public void testMenuFactoryPublicInterface() {
        int classModifiers = menuFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }
}
