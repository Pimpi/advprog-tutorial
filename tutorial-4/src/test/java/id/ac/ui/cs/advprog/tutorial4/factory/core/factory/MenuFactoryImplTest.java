package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class MenuFactoryImplTest {
    private Class<?> menuFactoryImplClass;
    private MenuFactoryImpl menuFactoryImpl;

    @BeforeEach
    public void setup() throws Exception {
        menuFactoryImplClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactoryImpl");
        menuFactoryImpl = new MenuFactoryImpl();
    }

    @Test
    public void testMenuFactoryImplIsAConcreteClass() {
        assertFalse(Modifier.isAbstract(menuFactoryImplClass.getModifiers()));
    }

    @Test
    public void testMenuFactoryImplIsAMenuFactory() {
        Collection<Type> interfaces = Arrays.asList(menuFactoryImplClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory")));
    }

    @Test
    public void testMenuFactoryImplMakeMenuInuzumaRamen() {
        Menu menu = menuFactoryImpl.makeMenu("mockName", "InuzumaRamen");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen",
                menu.getClass().getTypeName());
    }

    @Test
    public void testMenuFactoryImplMakeMenuLiyuanSoba() {
        Menu menu = menuFactoryImpl.makeMenu("mockName", "LiyuanSoba");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba",
                menu.getClass().getTypeName());
    }

    @Test
    public void testMenuFactoryImplMakeMenuMondoUdon() {
        Menu menu = menuFactoryImpl.makeMenu("mockName", "MondoUdon");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon",
                menu.getClass().getTypeName());
    }

    @Test
    public void testMenuFactoryImplMakeMenuSnevnezhaShirataki() {
        Menu menu = menuFactoryImpl.makeMenu("mockName", "SnevnezhaShirataki");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki",
                menu.getClass().getTypeName());
    }
}
