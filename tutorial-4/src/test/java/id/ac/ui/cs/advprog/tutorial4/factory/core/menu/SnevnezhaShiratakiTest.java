package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class SnevnezhaShiratakiTest {
    private Class<?> snevnezhaShiratakiClass;
    private Class<?> nameClass;
    private SnevnezhaShirataki snevnezhaShirataki;

    @BeforeEach
    public void setup() throws Exception {
        snevnezhaShiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki");
        nameClass = Class.forName("java.lang.String");
        snevnezhaShirataki = new SnevnezhaShirataki("mockName");
    }

    @Test
    public void testSnevnezhaShiratakiIsConcreteClass() {
        assertFalse(Modifier.isAbstract(snevnezhaShiratakiClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIsASubclassOfMenu() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu",
                snevnezhaShiratakiClass.getSuperclass().getTypeName());
    }

    @Test
    public void testSnevnezhaShiratakiConstructorReceivesNameAsParameter() {
        Class<?>[] classArg = new Class<?>[1];
        classArg[0] = nameClass;

        Collection<Constructor<?>> constructors = Arrays.asList(
                snevnezhaShiratakiClass.getDeclaredConstructors());

        assertTrue(constructors.stream()
                .anyMatch(type -> Arrays.equals(
                        type.getParameterTypes(), classArg)));
    }

    @Test
    public void testSnevnezhaShiratakiGetNameMethod() {
        assertEquals("java.lang.String", snevnezhaShirataki.getName().getClass().getTypeName());
        assertEquals("mockName", snevnezhaShirataki.getName());
    }

    @Test
    public void testSnevnezhaShiratakiGetNoodleMethod() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki",
                snevnezhaShirataki.getNoodle().getClass().getTypeName());
    }

    @Test
    public void testSnevnezhaShiratakiGetMeatMethod() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish",
                snevnezhaShirataki.getMeat().getClass().getTypeName());
    }

    @Test
    public void testSnevnezhaShiratakiGetToppingMethod() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower",
                snevnezhaShirataki.getTopping().getClass().getTypeName());
    }

    @Test
    public void testSnevnezhaShiratakiGetFlavorMethod() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami",
                snevnezhaShirataki.getFlavor().getClass().getTypeName());
    }
}
