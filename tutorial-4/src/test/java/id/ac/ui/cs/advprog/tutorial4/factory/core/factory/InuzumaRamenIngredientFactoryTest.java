package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class InuzumaRamenIngredientFactoryTest {
    private Class<?> inuzumaRamenIngredientFactoryClass;
    private InuzumaRamenIngredientFactory inuzumaRamenIngredientFactory;

    @BeforeEach
    public void setup() throws Exception {
        inuzumaRamenIngredientFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.InuzumaRamenIngredientFactory");
        inuzumaRamenIngredientFactory = new InuzumaRamenIngredientFactory();
    }

    @Test
    public void testInuzumaRamenIngredientFactoryIsConcreteClass() {
        assertFalse(Modifier.isAbstract(inuzumaRamenIngredientFactoryClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenIngredientFactoryIsAIngredientFactory() {
        Collection<Type> interfaces = Arrays.asList(inuzumaRamenIngredientFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientFactory")));
    }

    @Test
    public void testInuzumaRamenIngredientFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = inuzumaRamenIngredientFactoryClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testInuzumaRamenIngredientFactoryCreateNoodleReturnRamen() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen",
                inuzumaRamenIngredientFactory.createNoodle().getClass().getTypeName());
    }

    @Test
    public void testInuzumaRamenIngredientFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = inuzumaRamenIngredientFactoryClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testInuzumaRamenIngredientFactoryCreateMeatReturnPork() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork",
                inuzumaRamenIngredientFactory.createMeat().getClass().getTypeName());
    }

    @Test
    public void testInuzumaRamenIngredientFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = inuzumaRamenIngredientFactoryClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }

    @Test
    public void testInuzumaRamenIngredientFactoryCreateToppingReturnBoiledEgg() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg",
                inuzumaRamenIngredientFactory.createTopping().getClass().getTypeName());
    }

    @Test
    public void testInuzumaRamenIngredientFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = inuzumaRamenIngredientFactoryClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testInuzumaRamenIngredientFactoryCreateFlavorReturnSpicy() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy",
                inuzumaRamenIngredientFactory.createFlavor().getClass().getTypeName());
    }
}
