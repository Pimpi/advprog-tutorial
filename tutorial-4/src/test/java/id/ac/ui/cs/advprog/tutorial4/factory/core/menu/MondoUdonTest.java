package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class MondoUdonTest {
    private Class<?> mondoUdonClass;
    private Class<?> nameClass;
    private MondoUdon mondoUdon;

    @BeforeEach
    public void setup() throws Exception {
        mondoUdonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon");
        nameClass = Class.forName("java.lang.String");
        mondoUdon = new MondoUdon("mockName");
    }

    @Test
    public void testMondoUdonIsConcreteClass() {
        assertFalse(Modifier.isAbstract(mondoUdonClass.getModifiers()));
    }

    @Test
    public void testMondoUdonIsASubclassOfMenu() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu",
                mondoUdonClass.getSuperclass().getTypeName());
    }

    @Test
    public void testMondoUdonConstructorReceivesNameAsParameter() {
        Class<?>[] classArg = new Class<?>[1];
        classArg[0] = nameClass;

        Collection<Constructor<?>> constructors = Arrays.asList(
                mondoUdonClass.getDeclaredConstructors());

        assertTrue(constructors.stream()
                .anyMatch(type -> Arrays.equals(
                        type.getParameterTypes(), classArg)));
    }

    @Test
    public void testmondoUdonGetNameMethod() {
        assertEquals("java.lang.String", mondoUdon.getName().getClass().getTypeName());
        assertEquals("mockName", mondoUdon.getName());
    }

    @Test
    public void testmondoUdonGetNoodleMethod() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon",
                mondoUdon.getNoodle().getClass().getTypeName());
    }

    @Test
    public void testmondoUdonGetMeatMethod() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken",
                mondoUdon.getMeat().getClass().getTypeName());
    }

    @Test
    public void testmondoUdonGetToppingMethod() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese",
                mondoUdon.getTopping().getClass().getTypeName());
    }

    @Test
    public void testmondoUdonGetFlavorMethod() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty",
                mondoUdon.getFlavor().getClass().getTypeName());
    }
}
