package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class InuzumaRamenTest {
    private Class<?> inuzumaRamenClass;
    private Class<?> nameClass;
    private InuzumaRamen inuzumaRamen;

    @BeforeEach
    public void setup() throws Exception {
        inuzumaRamenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen");
        nameClass = Class.forName("java.lang.String");
        inuzumaRamen = new InuzumaRamen("mockName");
    }

    @Test
    public void testInuzumaRamenIsConcreteClass() {
        assertFalse(Modifier.isAbstract(inuzumaRamenClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenIsASubClassOfMenu() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu", inuzumaRamenClass.getSuperclass().getTypeName());
    }

    @Test
    public void testInuzumaRamenConstructorReceivesName() {
        Class<?>[] classArg = new Class[1];
        classArg[0] = nameClass;

        Collection<Constructor<?>> constructors = Arrays.asList(
                inuzumaRamenClass.getDeclaredConstructors());

        assertTrue(constructors.stream()
            .anyMatch(type -> Arrays.equals(
                    type.getParameterTypes(), classArg)));
    }

    @Test
    public void testInuzumaRamesGetNameMethod() {
        assertEquals("java.lang.String", inuzumaRamen.getName().getClass().getTypeName());
        assertEquals("mockName", inuzumaRamen.getName());
    }

    @Test
    public void testInuzumaRamenGetNoodleMethod() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen",
                inuzumaRamen.getNoodle().getClass().getTypeName());
    }

    @Test
    public void testInuzumaRamenGetMeatMethod() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork",
                inuzumaRamen.getMeat().getClass().getTypeName());
    }

    @Test
    public void testInuzumaRamenGetToppingMethod() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg",
                inuzumaRamen.getTopping().getClass().getTypeName());
    }

    @Test
    public void testInuzumaRamenGetFlavorMethod() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy",
                inuzumaRamen.getFlavor().getClass().getTypeName());
    }
}
