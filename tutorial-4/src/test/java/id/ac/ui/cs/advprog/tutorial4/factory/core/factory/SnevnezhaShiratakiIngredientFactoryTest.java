package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class SnevnezhaShiratakiIngredientFactoryTest {
    private Class<?> snevnezhaShiratakiIngredientFactoryClass;
    private SnevnezhaShiratakiIngredientFactory snevnezhaShiratakiIngredientFactory;

    @BeforeEach
    public void setup() throws Exception {
        snevnezhaShiratakiIngredientFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.SnevnezhaShiratakiIngredientFactory");
        snevnezhaShiratakiIngredientFactory = new SnevnezhaShiratakiIngredientFactory();
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryIsConcreteClass() {
        assertFalse(Modifier.isAbstract(snevnezhaShiratakiIngredientFactoryClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryIsAIngredientFactory() {
        Collection<Type> interfaces = Arrays.asList(snevnezhaShiratakiIngredientFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientFactory")));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = snevnezhaShiratakiIngredientFactoryClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryCreateNoodleReturnShirataki() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki",
                snevnezhaShiratakiIngredientFactory.createNoodle().getClass().getTypeName());
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = snevnezhaShiratakiIngredientFactoryClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryCreateMeat() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish",
                snevnezhaShiratakiIngredientFactory.createMeat().getClass().getTypeName());
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = snevnezhaShiratakiIngredientFactoryClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryCreateTopping() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower",
                snevnezhaShiratakiIngredientFactory.createTopping().getClass().getTypeName());
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = snevnezhaShiratakiIngredientFactoryClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryCreateFlavorReturnUmami() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami",
                snevnezhaShiratakiIngredientFactory.createFlavor().getClass().getTypeName());
    }
}
