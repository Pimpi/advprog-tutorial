package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class MondoUdonIngredientFactoryTest {
    private Class<?> mondoUdonIngredientFactoryClass;
    private MondoUdonIngredientFactory mondoUdonIngredientFactory;

    @BeforeEach
    public void setup() throws Exception {
        mondoUdonIngredientFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MondoUdonIngredientFactory");
        mondoUdonIngredientFactory = new MondoUdonIngredientFactory();
    }

    @Test
    public void testMondoUdonIngredientFactoryIsConcreteClass() {
        assertFalse(Modifier.isAbstract(mondoUdonIngredientFactoryClass.getModifiers()));
    }

    @Test
    public void testMondoUdonIngredientFactoryIsAIngredientFactory() {
        Collection<Type> interfaces = Arrays.asList(mondoUdonIngredientFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientFactory")));
    }

    @Test
    public void testMondoUdonIngredientFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = mondoUdonIngredientFactoryClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testMondoUdonIngredientFactoryCreateNoodleReturnUdon() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon",
                mondoUdonIngredientFactory.createNoodle().getClass().getTypeName());
    }

    @Test
    public void testMondoUdonIngredientFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = mondoUdonIngredientFactoryClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testMondoUdonIngredientFactoryCreateMeatReturnChicken() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken",
                mondoUdonIngredientFactory.createMeat().getClass().getTypeName());
    }

    @Test
    public void testMondoUdonIngredientFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = mondoUdonIngredientFactoryClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }

    @Test
    public void testMondoUdonIngredientFactoryCreateToppingReturnCheese() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese",
                mondoUdonIngredientFactory.createTopping().getClass().getTypeName());
    }

    @Test
    public void testMondoUdonIngredientFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = mondoUdonIngredientFactoryClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testMondoUdonIngredientFactoryCreateFlavorReturnSalty() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty",
                mondoUdonIngredientFactory.createFlavor().getClass().getTypeName());
    }
}
