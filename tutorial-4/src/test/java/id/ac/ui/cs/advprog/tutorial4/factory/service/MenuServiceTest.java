package id.ac.ui.cs.advprog.tutorial4.factory.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class MenuServiceTest {
    private Class<?> menuServiceClass;

    @BeforeEach
    public void setup() throws Exception {
        menuServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.service.MenuService");
    }

    @Test
    public void testMenuServicePublicInterface() {
        int classModifiers = menuServiceClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testMenuServiceHasGetMenusAbstractMethod() throws Exception {
        Method getMenus = menuServiceClass.getDeclaredMethod("getMenus");

        assertTrue(Modifier.isPublic(getMenus.getModifiers()));
        assertTrue(Modifier.isAbstract(getMenus.getModifiers()));
        assertEquals(0, getMenus.getParameterCount());
        assertEquals("java.util.List<id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu>",
                getMenus.getGenericReturnType().getTypeName());
    }
}
