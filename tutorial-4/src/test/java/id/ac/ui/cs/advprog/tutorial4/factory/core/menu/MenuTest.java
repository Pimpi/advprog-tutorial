package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class MenuTest {
    private Class<?> menuClass;
    private Class<?> nameClass;

    @BeforeEach
    public void setup() throws Exception {
        menuClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
        nameClass = Class.forName("java.lang.String");
    }

    @Test
    public void testMenuIsPublicAbstractClass() {
        int classModifiers = menuClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isAbstract(classModifiers));
    }

    @Test
    public void testMenuConstructorReceivesNameAsParameter() {
        Class<?>[] classArg = new Class[1];
        classArg[0] = nameClass;

        Collection<Constructor<?>> constructors = Arrays.asList(
                menuClass.getDeclaredConstructors());

        assertTrue(constructors.stream()
            .anyMatch(type -> Arrays.equals(
                    type.getParameterTypes(), classArg)));
    }

    @Test
    public void testMenuHasPrepareMethod() throws Exception {
        Method prepare = menuClass.getDeclaredMethod("prepare");

        assertEquals("void", prepare.getGenericReturnType().getTypeName());
        assertEquals(0, prepare.getParameterCount());
        assertTrue(Modifier.isPublic(prepare.getModifiers()));
    }

    @Test
    public void testMenuHasGetNameMethod() throws Exception {
        Method getName = menuClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String", getName.getGenericReturnType().getTypeName());
        assertEquals(0, getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testMenuHasGetNoodleMethod() throws Exception {
        Method getNoodle = menuClass.getDeclaredMethod("getNoodle");

        assertEquals(
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                getNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, getNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(getNoodle.getModifiers()));
    }

    @Test
    public void testMenuHasGetMeatMethod() throws Exception {
        Method getMeat = menuClass.getDeclaredMethod("getMeat");

        assertEquals(
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                getMeat.getGenericReturnType().getTypeName());
        assertEquals(0, getMeat.getParameterCount());
        assertTrue(Modifier.isPublic(getMeat.getModifiers()));
    }

    @Test
    public void testMenuHasGetToppingMethod() throws Exception {
        Method getTopping = menuClass.getDeclaredMethod("getTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
            getTopping.getGenericReturnType().getTypeName());
        assertEquals(0, getTopping.getParameterCount());
        assertTrue(Modifier.isPublic(getTopping.getModifiers()));
    }

    @Test
    public void testMenuHasGetFlavorMethod() throws  Exception{
        Method getFlavor = menuClass.getDeclaredMethod("getFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                getFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, getFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(getFlavor.getModifiers()));
    }
}
