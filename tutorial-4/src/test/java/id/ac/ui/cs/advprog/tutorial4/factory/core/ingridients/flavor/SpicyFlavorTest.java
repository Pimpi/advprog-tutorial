package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SpicyFlavorTest {
    private Class<?> spicyFlavorClass;

    @BeforeEach
    public void setup() throws Exception {
        spicyFlavorClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy");
    }

    @Test
    public void testSpicyFlavorIsConcreteClass() {
        assertFalse(Modifier.isAbstract(spicyFlavorClass.getModifiers()));
    }

    @Test
    public void testSpicyFlavorIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(spicyFlavorClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                    .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor")));
    }

    @Test
    public void testSpicyFlavorOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = spicyFlavorClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0, getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testSpicyFlavorGetDescription() {
        Spicy spicy = new Spicy();

        assertEquals("Adding Liyuan Chili Powder...", spicy.getDescription());
    }
}
