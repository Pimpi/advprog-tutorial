package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SweetFlavorTest {
    private Class<?> sweetFlavorClass;

    @BeforeEach
    public void setup() throws Exception {
        sweetFlavorClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet");
    }

    @Test
    public void testSweetFlavorIsConcreteClass() {
        assertFalse(Modifier.isAbstract(sweetFlavorClass.getModifiers()));
    }

    @Test
    public void testSweetFlavorIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(sweetFlavorClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor")));
    }

    @Test
    public void testSweetFlavorOverrideGetDescriptonMethod() throws Exception {
        Method getDescription = sweetFlavorClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0, getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testSweetFalvorGetDescription() {
        Sweet sweet = new Sweet();

        assertEquals("Adding a dash of Sweet Soy Sauce...", sweet.getDescription());
    }
}
