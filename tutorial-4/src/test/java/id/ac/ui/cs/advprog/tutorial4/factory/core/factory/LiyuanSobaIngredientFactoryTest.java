package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class LiyuanSobaIngredientFactoryTest {
    private Class<?> liyuanSobaIngredientFactoryClass;
    private LiyuanSobaIngredientFactory liyuanSobaIngredientFactory;

    @BeforeEach
    public void setup() throws Exception {
        liyuanSobaIngredientFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.LiyuanSobaIngredientFactory");
        liyuanSobaIngredientFactory = new LiyuanSobaIngredientFactory();
    }

    @Test
    public void testLiyuanSobaIngredientFactoryIsConcreteClass() {
        assertFalse(Modifier.isAbstract(liyuanSobaIngredientFactoryClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIngredientFactoryIsAIngredientFactory() {
        Collection<Type> interfaces = Arrays.asList(liyuanSobaIngredientFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientFactory")));
    }

    @Test
    public void testLiyuanSobaIngredientFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = liyuanSobaIngredientFactoryClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIngredientFactoryCreateNoodleReturnSoba() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba",
                liyuanSobaIngredientFactory.createNoodle().getClass().getTypeName());
    }

    @Test
    public void testLiyuanSobaIngredientFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = liyuanSobaIngredientFactoryClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIngredientFactoryCreateMeatReturnBeef() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef",
                liyuanSobaIngredientFactory.createMeat().getClass().getTypeName());
    }

    @Test
    public void testLiyuanSobaIngredientFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = liyuanSobaIngredientFactoryClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIngredientFactoryCreateToppingReturnMushroom() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom",
                liyuanSobaIngredientFactory.createTopping().getClass().getTypeName());
    }

    @Test
    public void testLiyuanSobaIngredientFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = liyuanSobaIngredientFactoryClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIngredientFactoryCreateFlavorReturnSweet() {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet",
                liyuanSobaIngredientFactory.createFlavor().getClass().getTypeName());
    }
}
