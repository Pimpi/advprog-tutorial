package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SaltyFlavorTest {
    private Class<?> saltyFlavorClass;

    @BeforeEach
    public void setup() throws Exception {
        saltyFlavorClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty");
    }

    @Test
    public void testSaltyFlavorIsConcreteClass() {
        assertFalse(Modifier.isAbstract(saltyFlavorClass.getModifiers()));
    }

    @Test
    public void testSaltyFlavorIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(saltyFlavorClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor")));
    }

    @Test
    public void testSaltyFlavorOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = saltyFlavorClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0, getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testSaltyFlavorGetDescription() {
        Salty salty = new Salty();

        assertEquals("Adding a pinch of salt...", salty.getDescription());
    }
}
