package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class OrderServiceTest {
    private Class<?> orderServiceClass;
    private OrderServiceImpl orderService;

    @BeforeEach
    public void setup() throws Exception {
        orderServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl");
        orderService = new OrderServiceImpl();
    }

    @Test
    public void testOrderServiceIsPublicConcrete() {
        assertTrue(Modifier.isPublic(orderServiceClass.getModifiers()));
        assertFalse(Modifier.isAbstract(orderServiceClass.getModifiers()));
    }

    @Test
    public void testOrderServiceOrderADrinkMethod() throws Exception {
        Method orderADrink = orderServiceClass.getDeclaredMethod("orderADrink", String.class);

        assertTrue(Modifier.isPublic(orderADrink.getModifiers()));
        assertEquals(1, orderADrink.getParameterCount());
        assertEquals("void",
                orderADrink.getGenericReturnType().getTypeName());
    }

    @Test
    public void testOrderServiceOrderADrinkInterruptedWhenCreateInstance() throws IllegalAccessException, InvocationTargetException, InstantiationException {
        Constructor<OrderDrink> constructor;
        constructor = (Constructor<OrderDrink>) OrderDrink.class.getDeclaredConstructors()[0];
        constructor.setAccessible(true);
        Thread.currentThread().interrupt();
        constructor.newInstance();
    }

    @Test
    public void testOrderServiceOrderAFoodInterruptedWhenCreateInstance() throws IllegalAccessException, InvocationTargetException, InstantiationException {
        Constructor<OrderFood> constructor;
        constructor = (Constructor<OrderFood>) OrderFood.class.getDeclaredConstructors()[0];
        constructor.setAccessible(true);
        Thread.currentThread().interrupt();
        constructor.newInstance();
    }

    @Test
    public void testOrderServiceOrderADrinkIsASameInstance() {
        orderService.orderADrink("mockDrink");
        OrderDrink orderDrink1 = orderService.getDrink();
        orderService.orderADrink("mockDrink2");
        OrderDrink orderDrink2 = orderService.getDrink();
        assertSame(orderDrink1, orderDrink2);
    }

    @Test
    public void testOrderServiceOrderAFoodMethod() throws Exception {
        Method orderAFood = orderServiceClass.getDeclaredMethod("orderAFood", String.class);

        assertTrue(Modifier.isPublic(orderAFood.getModifiers()));
        assertEquals(1, orderAFood.getParameterCount());
        assertEquals("void",
                orderAFood.getGenericReturnType().getTypeName());
    }

    @Test
    public void testOrderServiceOrderAFoodIsASameInstance() {
        orderService.orderAFood("mockFood1");
        OrderFood orderFood1 = orderService.getFood();
        orderService.orderAFood("mockFood2");
        OrderFood orderFood2 = orderService.getFood();
        assertSame(orderFood1, orderFood2);
    }

    @Test
    public void testOrderServiceGetDrinkMethod() throws Exception {
        Method getDrink = orderServiceClass.getDeclaredMethod("getDrink");

        assertTrue(Modifier.isPublic(getDrink.getModifiers()));
        assertEquals(0, getDrink.getParameterCount());
        assertEquals("id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink",
                getDrink.getGenericReturnType().getTypeName());
    }

    @Test
    public void testOrderServiceGetFoodMethod() throws Exception {
        Method getFood = orderServiceClass.getDeclaredMethod("getFood");

        assertTrue(Modifier.isPublic(getFood.getModifiers()));
        assertEquals(0, getFood.getParameterCount());
        assertEquals("id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood",
                getFood.getGenericReturnType().getTypeName());
    }

    @Test
    public void testOrderServiceGetDrink() {
        orderService.orderADrink("mockName");
        assertEquals("mockName", orderService.getDrink().getDrink());
    }

    @Test
    public void testOrderServiceGetFood() {
        orderService.orderAFood("mockName");
        assertEquals("mockName", orderService.getFood().getFood());
    }



}
