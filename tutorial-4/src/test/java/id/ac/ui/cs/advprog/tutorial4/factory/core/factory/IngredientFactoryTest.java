package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class IngredientFactoryTest {
    private Class<?> ingredientFactoryClass;

    @BeforeEach
    public void setup() throws Exception {
        ingredientFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientFactory");
    }

    @Test
    public void testIngredientFactoryPublicInterface() {
        int classModifiers = ingredientFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testIngredientFactoryHasCreateNoodleAbstractMethod() throws Exception {
        Method createNoodle = ingredientFactoryClass.getDeclaredMethod("createNoodle");
        int methodModifiers = createNoodle.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, createNoodle.getParameterCount());
    }

    @Test
    public void testIngredientFactoryHasCreateMeatAbstractMethod() throws Exception {
        Method createMeat = ingredientFactoryClass.getDeclaredMethod("createMeat");
        int methodModifiers = createMeat.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, createMeat.getParameterCount());
    }

    @Test
    public void testIngredientFactoryHasCreateToppingAbstractMethod() throws Exception {
        Method createTopping = ingredientFactoryClass.getDeclaredMethod("createTopping");
        int methodModifiers = createTopping.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, createTopping.getParameterCount());
    }

    @Test
    public void testIngredientFactoryHasCreateFlavorAbstractMethod() throws Exception {
        Method createFlavor = ingredientFactoryClass.getDeclaredMethod("createFlavor");
        int methodModifiers = createFlavor.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, createFlavor.getParameterCount());
    }
}
