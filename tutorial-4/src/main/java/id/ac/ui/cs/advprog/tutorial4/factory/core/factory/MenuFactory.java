package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;

public interface MenuFactory {
    public Menu makeMenu(String name, String type);
}
