package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.*;

public class MenuFactoryImpl implements MenuFactory {

    @Override
    public Menu makeMenu(String name, String type) {
        if (type.equals("InuzumaRamen")) {
            return new InuzumaRamen(name);
        } else if (type.equals("LiyuanSoba")) {
            return new LiyuanSoba(name);
        } else if (type.equals("MondoUdon")) {
            return new MondoUdon(name);
        } else {
            return new SnevnezhaShirataki(name);
        }
    }
}
