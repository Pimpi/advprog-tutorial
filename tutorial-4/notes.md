# Lazy and Eager Instantiation
## Lazy Instantiation
Lazy Instantiation adalah pembuatan objek yang dipanggil ketika objek pertama kali dibutuhkan. 

- Kelebihan
    - Hemat memori karena pembuatan objek hanya ketika dibutuhkan.
    
- Kekurangan
    - Ketika dibutuhkan, program akan memerika terlebih dahulu apakah instance sudah dibuat atau belum.
    
## Eager Instantiation
Eager Instantiation adalah pembuatan objek sejak awal runtime.

- Kelebihan
    - Waktu akses lebih cepat dibandingkan lazy instantiation karena tidak perlu memeriksa instance sudah dibuat atau belum.
    
- Kekurangan
    - Memakan memori sejak awal runtime.