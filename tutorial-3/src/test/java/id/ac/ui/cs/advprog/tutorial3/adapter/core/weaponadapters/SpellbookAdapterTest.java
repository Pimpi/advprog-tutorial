package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Heatbearer;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.TheWindjedi;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class SpellbookAdapterTest {
    private Class<?> spellbookAdapterClass;
    private Class<?> spellbookClass;

    @BeforeEach
    public void setUp() throws Exception {
        spellbookAdapterClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter");
        spellbookClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook");
    }

    @Test
    public void testSpellbookAdapterIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(spellbookAdapterClass.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterIsAWeapon() {
        Collection<Type> interfaces = Arrays.asList(spellbookAdapterClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon")));
    }

    @Test
    public void testSpellbookAdapterConstructorReceivesSpellbookAsParameter() {
        Class<?>[] classArg = new Class[1];
        classArg[0] = spellbookClass;
        Collection<Constructor<?>> constructors = Arrays.asList(
                spellbookAdapterClass.getDeclaredConstructors());

        assertTrue(constructors.stream()
                .anyMatch(type -> Arrays.equals(
                        type.getParameterTypes(), classArg)));
    }

    @Test
    public void testSpellbookAdapterOverrideNormalAttackMethod() throws Exception {
        Method normalAttack = spellbookAdapterClass.getDeclaredMethod("normalAttack");

        assertEquals("java.lang.String",
                normalAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                normalAttack.getParameterCount());
        assertTrue(Modifier.isPublic(normalAttack.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterOverrideChargedAttackMethod() throws Exception {
        Method chargedAttack = spellbookAdapterClass.getDeclaredMethod("chargedAttack");

        assertEquals("java.lang.String",
                chargedAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                chargedAttack.getParameterCount());
        assertTrue(Modifier.isPublic(chargedAttack.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterOverrideGetNameMethod() throws Exception {
        Method getName = spellbookAdapterClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterOverrideGetHolderMethod() throws Exception {
        Method getHolderName = spellbookAdapterClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterHeatbearerGetName() {
        Heatbearer mockHeatbearer = new Heatbearer("mockHolderName");
        SpellbookAdapter mockHeatbearerAdapter = new SpellbookAdapter(mockHeatbearer);

        assertEquals(mockHeatbearer.getName(), mockHeatbearerAdapter.getName());
    }

    @Test
    public void testSpellbookAdapterTheWindjediGetName() {
        TheWindjedi mockTheWindjedi = new TheWindjedi("mockHolderName");
        SpellbookAdapter mockTheWindjediAdapter = new SpellbookAdapter(mockTheWindjedi);

        assertEquals(mockTheWindjedi.getName(), mockTheWindjediAdapter.getName());
    }

    @Test
    public void testSpellbookAdapterHeatbearerGetHolderName() {
        Heatbearer mockHeatbearer = new Heatbearer("mockHolderName");
        SpellbookAdapter mockHeatbearerAdapter = new SpellbookAdapter(mockHeatbearer);

        assertEquals(mockHeatbearer.getHolderName(), mockHeatbearerAdapter.getHolderName());
    }

    @Test
    public void testSpellbookAdapterTheWindjediGetHolderName() {
        TheWindjedi mockTheWindjedi = new TheWindjedi("mockHolderName");
        SpellbookAdapter mockTheWindjediAdapter = new SpellbookAdapter(mockTheWindjedi);

        assertEquals(mockTheWindjedi.getHolderName(), mockTheWindjediAdapter.getHolderName());
    }

    @Test
    public void testSpellbookAdapterHeatbearerNormalAttack() {
        Heatbearer mockHeatbearer = new Heatbearer("mockHolderName");
        SpellbookAdapter mockHeatbearerAdapter = new SpellbookAdapter(mockHeatbearer);

        assertEquals(mockHeatbearer.smallSpell(), mockHeatbearerAdapter.normalAttack());
    }

    @Test
    public void testSpellbookAdapterTheWindjediNormalAttack() {
        TheWindjedi mockTheWindjedi = new TheWindjedi("mockHolderName");
        SpellbookAdapter mockTheWindjediAdapter = new SpellbookAdapter(mockTheWindjedi);

        assertEquals(mockTheWindjedi.smallSpell(), mockTheWindjediAdapter.normalAttack());
    }

    @Test
    public void testSpellbookAdapterHeatbearerChargedAttack() {
        Heatbearer mockHeatbearer = new Heatbearer("mockHolderName");
        SpellbookAdapter mockHeatbearerAdapter = new SpellbookAdapter(mockHeatbearer);

        assertEquals(mockHeatbearer.largeSpell(), mockHeatbearerAdapter.chargedAttack());
        assertEquals("Too tired...", mockHeatbearerAdapter.chargedAttack());
        mockHeatbearerAdapter.normalAttack();
        assertEquals(mockHeatbearer.largeSpell(), mockHeatbearerAdapter.chargedAttack());
    }

    @Test
    public void testSpellbookAdapterTheWindjediChargedAttack() {
        TheWindjedi mockTheWindjedi = new TheWindjedi("mockHolderName");
        SpellbookAdapter mockTheWindjediAdapter = new SpellbookAdapter(mockTheWindjedi);

        assertEquals(mockTheWindjedi.largeSpell(), mockTheWindjediAdapter.chargedAttack());
        assertEquals("Too tired...", mockTheWindjediAdapter.chargedAttack());
        mockTheWindjediAdapter.normalAttack();
        assertEquals(mockTheWindjedi.largeSpell(), mockTheWindjediAdapter.chargedAttack());
    }
}
