package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.IonicBow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.UranosBow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class BowAdapterTest {
    private Class<?> bowAdapterClass;
    private Class<?> bowClass;

    @BeforeEach
    public void setUp() throws Exception {
        bowAdapterClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter");
        bowClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow");
    }

    @Test
    public void testBowAdapterIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(bowAdapterClass.getModifiers()));
    }

    @Test
    public void testBowAdapterIsAWeapon() {
        Collection<Type> interfaces = Arrays.asList(bowAdapterClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon")));
    }

    @Test
    public void testBowAdapterConstructorReceivesBowAsParameter() {
        Class<?>[] classArg = new Class[1];
        classArg[0] = bowClass;
        Collection<Constructor<?>> constructors = Arrays.asList(
                bowAdapterClass.getDeclaredConstructors());

        assertTrue(constructors.stream()
                .anyMatch(type -> Arrays.equals(
                        type.getParameterTypes(), classArg)));
    }

    @Test
    public void testBowAdapterOverrideNormalAttackMethod() throws Exception {
        Method normalAttack = bowAdapterClass.getDeclaredMethod("normalAttack");

        assertEquals("java.lang.String",
                normalAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                normalAttack.getParameterCount());
        assertTrue(Modifier.isPublic(normalAttack.getModifiers()));
    }

    @Test
    public void testBowAdapterOverrideChargedAttackMethod() throws Exception {
        Method chargedAttack = bowAdapterClass.getDeclaredMethod("chargedAttack");

        assertEquals("java.lang.String",
                chargedAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                chargedAttack.getParameterCount());
        assertTrue(Modifier.isPublic(chargedAttack.getModifiers()));
    }

    @Test
    public void testBowAdapterOverrideGetNameMethod() throws Exception {
        Method getName = bowAdapterClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testBowAdapterOverrideGetHolderMethod() throws Exception {
        Method getHolderName = bowAdapterClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    @Test
    public void testBowAdapterIonicBowGetName() {
        IonicBow mockIonicBow = new IonicBow("mockHolderName");
        BowAdapter mockIonicBowAdapter = new BowAdapter(mockIonicBow);

        assertEquals(mockIonicBow.getName(), mockIonicBowAdapter.getName());
    }

    @Test
    public void testBowAdapterUranosBowGetName() {
        UranosBow mockUranosBow = new UranosBow("mockHolderName");
        BowAdapter mockUranosBowAdapter = new BowAdapter(mockUranosBow);

        assertEquals(mockUranosBow.getName(), mockUranosBowAdapter.getName());
    }

    @Test
    public void testBowAdapterIonicBowGetHolderName() {
        IonicBow mockIonicBow = new IonicBow("mockHolderName");
        BowAdapter mockIonicBowAdapter = new BowAdapter(mockIonicBow);

        assertEquals(mockIonicBow.getHolderName(), mockIonicBowAdapter.getHolderName());
    }

    @Test
    public void testBowAdapterUranosBowGetHolderName() {
        UranosBow mockUranosBow = new UranosBow("mockHolderName");
        BowAdapter mockUranosBowAdapter = new BowAdapter(mockUranosBow);

        assertEquals(mockUranosBow.getHolderName(), mockUranosBowAdapter.getHolderName());
    }

    @Test
    public void testBowAdapterIonicBowChargedAttack() {
        IonicBow mockIonicBow = new IonicBow("mockHolderName");
        BowAdapter mockIonicBowAdapter = new BowAdapter(mockIonicBow);

        assertEquals("Aiming mode", mockIonicBowAdapter.chargedAttack());

        assertEquals("Spontaneous mode", mockIonicBowAdapter.chargedAttack());
    }

    @Test
    public void testBowAdapterUranosBowChargedAttack() {
        UranosBow mockUranosBow = new UranosBow("mockHolderName");
        BowAdapter mockUranosBowAdapter = new BowAdapter(mockUranosBow);

        assertEquals("Aiming mode", mockUranosBowAdapter.chargedAttack());

        assertEquals("Spontaneous mode", mockUranosBowAdapter.chargedAttack());
    }

    @Test
    public void testBowAdapterIonicBowNormalAttack() {
        IonicBow mockIonicBow = new IonicBow("mockHolderName");
        BowAdapter mockIonicBowAdapter = new BowAdapter(mockIonicBow);

        assertEquals(mockIonicBow.shootArrow(false), mockIonicBowAdapter.normalAttack());

        mockIonicBowAdapter.chargedAttack();

        assertEquals(mockIonicBow.shootArrow(true), mockIonicBowAdapter.normalAttack());
    }

    @Test
    public void testBowAdapterUranosBowNormalAttack() {
        UranosBow mockUranosBow = new UranosBow("mockHolderName");
        BowAdapter mockUranosBowAdapter = new BowAdapter(mockUranosBow);

        assertEquals(mockUranosBow.shootArrow(false), mockUranosBowAdapter.normalAttack());

        mockUranosBowAdapter.chargedAttack();

        assertEquals(mockUranosBow.shootArrow(true), mockUranosBowAdapter.normalAttack());
    }
}
