package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;

/**
 * Kelas ini melakukan substitusi huruf dengan 13 huruf selanjutnya secara alfabetikal
 */
public class Rot13Transformation {

    public Spell encode(Spell spell) {
        return process(spell);
    }

    public Spell decode(Spell spell) {
        return process(spell);
    }

    private Spell process(Spell spell) {
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int n = text.length();
        char[] res = new char[n];
        for (int i = 0; i < n; i++) {
            char c = text.charAt(i);
            if       (c >= 'a' && c <= 'm') c += 13;
            else if  (c >= 'A' && c <= 'M') c += 13;
            else if  (c >= 'n' && c <= 'z') c -= 13;
            else if  (c >= 'N' && c <= 'Z') c -= 13;
            res[i] = c;
        }

        return new Spell(new String(res), codex);
    }
}
