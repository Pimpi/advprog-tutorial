package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean hasCast;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.hasCast = false;
    }

    @Override
    public String normalAttack() {
        hasCast = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (hasCast) {
            return "Too tired...";
        }
        hasCast = true;
        return spellbook.largeSpell();
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
