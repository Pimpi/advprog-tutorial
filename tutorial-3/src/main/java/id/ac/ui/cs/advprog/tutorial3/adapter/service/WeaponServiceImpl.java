package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    private boolean isWeaponSaved = false;

    private void saveAllWeapon() {
        for (Bow bow : this.bowRepository.findAll()) {
            this.weaponRepository.save(new BowAdapter(bow));
        }

        for (Spellbook spellbook : this.spellbookRepository.findAll()) {
            this.weaponRepository.save(new SpellbookAdapter(spellbook));
        }

        this.isWeaponSaved = true;
    }

    @Override
    public List<Weapon> findAll() {
        if (!this.isWeaponSaved) {
            saveAllWeapon();
        }
        return this.weaponRepository.findAll();
    }

    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        if (!this.isWeaponSaved) {
            saveAllWeapon();
        }
        Weapon weapon = this.weaponRepository.findByAlias(weaponName);
        this.weaponRepository.save(weapon);
        if (weapon != null) {
            if (attackType == 0) {
                this.logRepository.addLog(String.format("%s attacked with %s (%s): %s", weapon.getHolderName(), weapon.getName(), "Normal Attack", weapon.normalAttack()));
            } else if (attackType == 1) {
                this.logRepository.addLog(String.format("%s attacked with %s (%s): %s", weapon.getHolderName(), weapon.getName(), "Charged Attack", weapon.chargedAttack()));
            }
        }
    }

    @Override
    public List<String> getAllLogs() {
        if (!this.isWeaponSaved) {
            saveAllWeapon();
        }
        return this.logRepository.findAll();
    }
}
